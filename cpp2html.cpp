/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 * Gerry Xu: 8 hours
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.

// enumeration for our highlighting tags:
enum
{
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap =
{
#include "res/keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans =
{
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c)
{
	switch (c)
	{
		case '"':
			return "&quot;";
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}

string processOneLine(string line); // Declared a function called processOneLine.
int main()
{
	// TODO: write the main program.
	// It may be helpful to break this down and write
	// a function that processes a single line, which
	// you repeatedly call from main().
	string userInput;
	while(getline(cin, userInput))
	{
		cout << processOneLine(userInput) << endl;
	}
	return 0;
}

string processOneLine(string line)
{
	string returnValue = "";
	int state = start;
	for(unsigned int i = 0; i < line.length(); i++)
	{
		string output = "";
		updateState(state,line[i]);

		switch(state)
		{
			case start:
				while(state == start)
				{
					output += translateHTMLReserved(line[i]);
					i++;
					updateState(state,line[i]);
					{
						if(i == line.length())
						{
							break;
						}
					}
					returnValue += output;
					output.clear();
					break;
				}
			case scanid:
				while(state == scanid)
				{
					output += line[i];
					i++;
					updateState(state,line[i]);
				}
				if(state != 1)
				{
					map<string, short>::iterator it;
					it = hlmap.find(output);
					if(it != hlmap.end())
					{
						returnValue += hlspans[hlmap[output]] + output + spanend + translateHTMLReserved(line[i]);
					}
					else
					{
						returnValue += output + translateHTMLReserved(line[i]);

					}
				}
				break;

			case readesc:
				output += translateHTMLReserved(line[i]);
				i++;
				if(line[i] == 'n')
				{
					output += line[i];
					returnValue += hlspans[hlescseq] + output + spanend;
					output.clear();
					state = strlit;
				}
				else
				{
					output += line[i];
					returnValue += hlspans[hlerror] + output + spanend;
					output.clear();
				}
				output.clear();
				break;

			case strlit:
				output += line[i];
			  returnValue += hlspans[hlstrlit] + output + spanend;
			  break;

			case scannum:
				while(state == scannum)
				{
					char tempint = line[i];
					output += tempint;
					i++;
					updateState(state,line[i]);
				}
				returnValue += hlspans[hlnumeric] + output + spanend;
				output.clear();
				break;

			case comment:
				returnValue +=line[i];
				if(i == line.length()-1)
				{
					returnValue += spanend;
				}
				break;

			case error:
				output += line[i];
				returnValue += hlspans[hlerror] + output + spanend;
				break;

			case readfs:
				returnValue += hlspans[hlcomment] + line[i];
				state = comment;
		}
	}
	return returnValue;
}